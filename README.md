<div align="center">
<h1>TELEPORT TO LOCATION</h1></div>
This is simple resource that allows you to teleport to specific location. This resource is for RAGEMP servers. But can easily be :ship: to another servers. 

## Description :page_facing_up:
Have you used `/tp` to teleport to specific coords ?. It was cool right?. But typing everytime the same coords to teleport to location is boring. 
So here comes this simple script that alows you to teleport to location without typing coords, more about [here](#usage)

## Instalation :rocket:
Instalation is realy simple. Only thing you need to do is place folder `teleport-to-location` to your `server-files/packages` folder and add

```js
// Main index.js
require('teleport-to-location');
```
to your main `index.js` file inside `packages` folder.


## Usage
This simple script allows you to teleport to specific location like for example Los Santos Airport using command like:

```js
/tpto lsairport // This will teleport you to los santos airport
```

Locations available are stored in `location.js` file. Custom locations are availalbe to add within `custom.js` file.

## Preview
![preview](preview.png)

This command will teleport you to observatory 

## Add custom locations :pencil:
Only thing you need to do is locate `custom.js` file in project directory and add (you need to replace `<>` with your values, this is only placeholder!): 

```js
{
    name: '<name>',
    position: new mp.Vector3(<x>, <y>, <z>)
},
```

If you planned to :heavy_plus_sign: some new locations to this script you can use [this](https://gitlab.com/-/snippets/2016661) snippet for :raised_hand: or reference.

If you :heart: or :thumbsup: this resource, don't forget to leave :star: on this repo or :pencil2: nice comment on [forum](rage.mp/forums). If you want to :heavy_plus_sign: something to this script or found a :bug:, don't hasitate to write to me on [forum](rage.mp/forums) or open issue in this [repo](gitlab.com/Czesiek2000/teleport-to-location). 

Thanks :punch:,  enjoy this resource.
