let locations = [
    {
        name: 'rathouse',
        positon: new mp.Vector3(-543.14551, -207.20975, 37.64981),
        heading: -156.0560
    },
    {
        name: 'observatory',
        position: new mp.Vector3(-425.49841, 1123.39319, 325.85446),
        heading: -16.122
    },
    {
        name: 'humanlabs',
        position: new mp.Vector3(3571.26904296875, 3787.811767578125, 29.86764144897461),
    },
    {
        name: 'lsairport',
        position: new mp.Vector3(-985.4199829101562, -2827.7861328125, 13.128203392028809),
        heading: -146.5106
    },
    {
        name: 'mckenzie',
        position: new mp.Vector3(2126.814697265625, 4817.1337890625, 41.1511344909668)
    },
    {
        name: 'fortzankudo',
        position: new mp.Vector3(-2449.682373046875, 2960.828857421875, 32.6905403137207),
        heading: -31.4205
    },
    {
        name: 'docs',
        position: new mp.Vector3(1272.3798828125, -3301.85302734375, 5.775706768035889)
    },
    {
        name: 'grovestreet',
        position: new mp.Vector3(106.94109344482422, -1940.130615234375, 19.968420028686523)
    },
    {
        name: 'lsplaza',
        position: new mp.Vector3(-1472.7396240234375, -1450.168701171875, 2.1504056453704834 )
    },
    {
        name: 'casino',
        position: new mp.Vector3(918.2734375, 52.080726623535156, 80.83312225341797),
        heading: -129.2588
    },
    {
        name: 'winiary',
        position: new mp.Vector3(-1902.1910400390625, 2033.869873046875, 140.68060302734375)
    },
    {
        name: 'vinewoodhill',
        position: new mp.Vector3(789.6124877929688, 1283.61083984375, 360.2142333984375)
    },
    {
        name: 'sandyairport',
        position: new mp.Vector3(1741.767822265625, 3263.849853515625, 41.1775016784668)
    },
    {
        name: 'paleto',
        position: new mp.Vector3(-382.0462646484375, 5995.0595703125, 31.343059539794922)
    },
    {
        name: 'molo',
        position: new mp.Vector3(-1846.3438720703125, -1232.3707275390625, 12.343626022338867),
        heading: 130.1649
    },
    {
        name: 'mazebank',
        position: new mp.Vector3(-71.92085266113281, -817.9440307617188, 326.048583984375)
    },
    {
        name: 'cluckinbell',
        position: new mp.Vector3(-22.623952865600586, 6304.4248046875, 31.374719619750977),
        heading: 29.3702
    },
    {
        name: 'cementary',
        position: new mp.Vector3(-1739.7882080078, -240.49043273926, 53.510799407959),
        heading: 158.3831
    },
    {
        name: 'construction',
        position: new mp.Vector3(48.3131217956543, -398.241455078125, 64.66389465332031),
        heading: -102.3355
    },
    {
        name: 'lsparking',
        position: new mp.Vector3(-491.84457397461, 166.80014038086, 70.817123413086 )
    },
    {
        name: 'redwoodlight',
        position: new mp.Vector3(1032.185546875, 2281.1052246094, 49.182479858398)
    },
    {
        name: 'sawmill',
        position: new mp.Vector3(-593.1275024414062, 5298.279296875, 69.3794937133789),
        heading: -102.3355
    },
    {
        name: 'altruist',
        position: new mp.Vector3(-1128.6850585938, 4925.6743164063, 219.3360748291)
    },
    {
        name: 'mountchilliad',
        position: new mp.Vector3(501.22662353515625, 5603.837890625, 797.9100952148438),
        heading: 170.082916
    },
    {
        name: 'golfclub',
        position: new mp.Vector3(-1363.6138916015625, -12.109723091125488, 53.479026794433594)
    },
    {
        name: 'oneils',
        position: new mp.Vector3(2469.8154296875, 4955.7998046875, 45.1077995300293),
        heading: 60.0177
    },
    {
        name: 'farm',
        position: new mp.Vector3(1991.666259765625, 4872.61181640625, 43.61996078491211)
    },
    {
        name: 'planetary',
        position: new mp.Vector3(-2343.397705078125, 269.51171875, 169.3555450439453)
    },
    {
        name: 'noose',
        position: new mp.Vector3(2562.839599609375, -426.2480773925781, 92.87579345703125)
    },
    {
        name: 'powerstation',
        position: new mp.Vector3(2726.314697265625, 1337.743408203125, 24.390827178955078)
    },
    {
        name: 'canals',
        position: new mp.Vector3(902.3670654296875, -388.9021911621094, 36.5599479675293)
    },
    {
        name: 'prison',
        position: new mp.Vector3(1854.9501953125, 2609.4501953125, 45.60691452026367)
    },
    {
        name: 'madrazzoranch',
        position: new mp.Vector3(1355.361083984375, 1143.03076171875, 113.64070892333984)
    },
    {
        name: 'finalmission',
        position: new mp.Vector3(1069.869384765625, -1959.2525634765625, 31.010643005371094)
    },
    {
        name: 'vinewoode',
        position: new mp.Vector3(711.1280517578125, 1198.2425537109375, 348.52685546875),
        heading: 165.2529
    },
    {
        name: 'theater',
        position: new mp.Vector3(672.2835693359375, 504.3349914550781, 134.93531799316406),
        heading: -13.7906
    },
    {
        name: 'electricity',
        position: new mp.Vector3(711.647705078125, 143.63880920410156, 80.61234283447266)
    },
    {
        name: 'windfarm',
        position: new mp.Vector3(2271.199951171875, 2172.37890625, 78.14751434326172)
    },
    {
        name: 'cinema',
        position: new mp.Vector3(299.2770080566406, 191.69332885742188, 104.12120819091797),
        heading: 157.6783
    },
    {
        name: 'michealhouse',
        position: new mp.Vector3(-803.0276489257812, 171.55084228515625, 72.84464263916016)
    },
    {
        name: 'franklinhouse',
        position: new mp.Vector3(-10.256678581237793, 517.6415405273438, 174.6280059814453)
    },
    {
        name: 'trevorhouse',
        position: new mp.Vector3(1971.4783935546875, 3816.79052734375, 32.9428825378418)
    },
    {
        name: 'stripclub',
        position: new mp.Vector3(128.15313720703125, -1290.5850830078125, 29.26953125)
    },
    {
        name: 'lesterhouse',
        position: new mp.Vector3(1274.68505859375, -1711.3798828125, 54.771453857421875)
    },
    {
        name: 'teuquila',
        position: new mp.Vector3(-555.1599731445312, 284.9700012207031, 82.16999816894531)
    },
    {
        name: 'lostshq',
        position: new mp.Vector3(986.7827758789062, -98.1712646484375, 74.84619140625)
    },
    {
        name: 'highinsky',
        position: new mp.Vector3(13.845671653747559, -978.9203491210938, 2698.150146484375)
    },
    {
        name: 'aircraft',
        position: new mp.Vector3(3082.31201171875, -4717.119140625, 15.262274742126465)
    },
    {
        name: 'yacht',
        position: new mp.Vector3(-2043.9739990234375, -1031.58203125, 11.980844497680664)
    },
    {
        name: 'arenaroof',
        position: new mp.Vector3(-324.300, -1968.545, 67.002)
    },
    {
        name: 'mine',
        position: new mp.Vector3(-595.342, 2086.008, 131.412)
    }
]

module.exports = locations;
