const locations = require('./locations')
const custom = require('./custom');
let data;
if (custom.length != 0) {
	data = [...locations, ...custom];
} else {
	data = locations;
}
mp.events.addCommand("tpto", (player, _, location) => {
	if (location == undefined || location.length === 0) {
		player.outputChatBox("tpto [location]");
		return;
	} else {
		let l = data.filter((n) => n.name == location).reduce((a) => a);
		player.position = new mp.Vector3(l.x, l.y, l.z);
		player.heading = l.heading;
		player.outputChatBox("Teleported to " + l.name);
	}
});
